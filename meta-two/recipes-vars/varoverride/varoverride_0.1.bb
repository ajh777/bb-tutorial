DESCRIPTION = "Show var overrides"
PR = "r1"

OVERRIDES = "foo"

# Example 1
A = "Z"
A_foo_append = "X"

# Example 2
B = "Z"
B_append_foo = "X"

# Example 3
C = "Y"
C_foo_append = "Z"
C_foo_append = "X"

# Example 4
D = "1"
D_append = "2"
D_append = "3"
# Immediate operator
D += "4"
D .= "5"

do_build() {
  echo "A: ${A}" # X
  echo "B: ${B}" # ZX
  echo "C: ${C}" # ZX
  echo "D: ${D}" # 1 4523
}
