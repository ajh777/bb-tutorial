Content:
```
bb-tutorial
|-- build
|   `-- conf
|       |-- bblayers.conf                     Required for bb
|       `-- local.conf
|-- meta-tutorial                             A layer
|   |-- classes
|   |   |-- base.bbclass                      A copy from bb installation dir
|   |   `-- mybuild.bbclass
|   |-- conf
|   |   |-- bitbake.conf                      A copy from bb installation dir
|   |   `-- layer.conf                        Each layer needs such a file
|   `-- recipes-tutorial
|       |-- first
|       |   `-- first_0.1.bb
|       `-- second
|           `-- second_1.0.bb
`-- meta-two                                  Another layer
    |-- classes
    |   |-- confbuild.bbclass
    |   `-- varbuild.bbclass
    |-- conf
    |   `-- layer.conf                        Each layer needs such a file
    |-- recipes-base
    |   |-- first
    |   |   `-- first_0.1.bbappend
    |   `-- third
    |       `-- third_01.bb
    `-- recipes-vars
        |-- myvar
        |   `-- myvar_0.1.bb
        `-- varbuild
            `-- varbuild_0.1.bb
```