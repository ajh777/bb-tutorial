DESCRIPTION = "I am the second recipe."
PR = "r1"

# The mybuild class becomes inherited and 
# so myclass_do_build becomes the default task
inherit mybuild

# A pure python function
def pyfunc(o):
  print(dir(o))

python do_mypatch() {
  bb.note("Running task mypatch")
  pyfunc(d)
}

addtask mypatch before do_build
